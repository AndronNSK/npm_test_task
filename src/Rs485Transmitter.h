#ifndef RS485TRANSMITTER_H
#define RS485TRANSMITTER_H

#include "stm8s.h"

#include "string.h"


/*
 * Packet format:
 * -----------------------------------------------------------------------------------------------------------
 * | Address (1 byte) | Command Id (1 byte) | Data Amount (1 byte) | Data (0...32 bytes) | CRC 16 (2 bytes)  |
 * -----------------------------------------------------------------------------------------------------------
 * When transmitting, SLIP protocol rules are applied. Flags are added at start and the end of packet.
 *
 * Commands are application-defined and can be any. Slave responds to any messages for him (excepting broadcast, if it's present)
 *
 * */

typedef int8_t DataLength_t;
typedef int8_t CommandId_t;
typedef int8_t DeviceId_t;
typedef uint32_t TickCounter_t;
typedef uint16_t CrcValue_t;

#define MAX_DATA_COUNT          32                      /* Max amount of data in packet */
#define MAX_PACKET_LENGTH       (32 * 2 + 1 + 1 + 2)    /* Max length of raw data */
#define RS485_ANSWER_TIMEOUT    1000                    /* Timeout in some time units */

#define SLIP_END    0xC0      /* Frame End */
#define SLIP_ESC    0xDB      /* Frame Escape */
#define ESC_END     0xDC      /* Transposed Frame End */
#define ESC_ESC     0xDD      /* Transposed Frame Escape */

#define CRC_INITAL_VALUE    0xFF    /* Initial value of CRC value, depends on chosen algorithm. 0xFF as an example */

enum Result_e {
    RESULT_OK = 0,
    RESULT_INVALID_CRC = -1,
    RESULT_TIMEOUT = -2,
};

/* State Machine of Packet Parser */
enum PacketParserState_e {
    PARSER_STATE_INITIAL,       /* Initial state, looking for a start symbol */
    PARSER_STATE_STARTED,       /* State when a packet started and ordinary data is being received */
    PARSER_STATE_ESCAPE_FOUND,  /* When a first byte of escape id found */

};

/**
 * Role of transmitter
 */
enum Rs485TransmitterRole_e
{
    ROLE_MASTER,//!< ROLE_MASTER
    ROLE_SLAVE, //!< ROLE_SLAVE
};

/**
 * Stub for CRC16 function
 * @param initalCrc     Initial CRC value
 * @param pData         data for calculation
 * @param dataLength    amount of data
 * @return  CRC value
 */
extern uint16_t crc16(uint16_t initalCrc, const void *pData, size_t dataLength);

struct Rs485Packet
{
    DeviceId_t deviceId;
    CommandId_t cmd;
    void *pData;
    DataLength_t bytesCount;
};

/**
 * Function stub to get current tick counter, to measure time intervals
 * @return  current tick counter
 */
extern TickCounter_t getCurrentTicks();

class Rs485PacketDecoder
{
public:
    Rs485PacketDecoder(uint8_t *pBuffer, DataLength_t bufferSize) :
        bufferSize_(bufferSize), pBuffer_(pBuffer)
    {
        Initialize();
    }

    void Initialize()
    {
        state_ = PARSER_STATE_INITIAL;
        isPacketReady_ = false;
        packetLength_ = 0;
    }

    /**
     * Puts next byte to decoded packet. Should be called when new byte is got.
     * @param byte  byte from line
     */
    void PutByte(uint8_t byte)
    {
        /**
         * State machine for SLIP decoder
         */
        switch(state_)
        {
        case PARSER_STATE_INITIAL:
            if ( byte == SLIP_END )
            {
                state_ = PARSER_STATE_STARTED;
            }
            break;
        case PARSER_STATE_STARTED:
            if ( byte == SLIP_ESC ) {
                state_ = PARSER_STATE_ESCAPE_FOUND;
            }
            else if ( byte == SLIP_END )
            {
                state_ = PARSER_STATE_STARTED;
                if ( packetLength_ > 0 )
                {
                    isPacketReady_ = true;
                }
            }
            else
            {
                *(pBuffer_ + packetLength_) = byte;
                packetLength_++;
            }
            break;
        case PARSER_STATE_ESCAPE_FOUND:
            if ( byte == ESC_END )
            {
                *(pBuffer_ + packetLength_) = SLIP_END;
                packetLength_++;
            }
            else if ( byte == ESC_ESC )
            {
                *(pBuffer_ + packetLength_) = SLIP_ESC;
                packetLength_++;
            }
            break;
        default:
            /* TODO Here should be an assert with fault condition */
            while(1);
        }
    }

    /**
     * Returns true when packet received
     * @param pLength   length of packet
     * @return  true if packet ready
     */
    bool GetPacketLength(DataLength_t* pLength)
    {
        /* TODO Disable interrupts */
        bool isReady = isPacketReady_;
        *pLength = isReady ? packetLength_ : 0;
        /* TODO Enable Interrupts */

        return isReady;
    }

protected:

    bool isPacketReady_;
    DataLength_t packetLength_;
    PacketParserState_e state_;

    uint8_t * const pBuffer_;
    const DataLength_t bufferSize_;
};


/**
 * Represents class to transmit data over UART (RS485)
 */
class Rs485Transmitter
{
public:

    /**
     * Sends packet, waits for answer. Blocking implementation. For unblocked implementation should be
     * overridden in a nested class and exclude awaiting of received packet
     * @param deviceId      address or Id of device
     * @param cmd           command Id
     * @param pData         data to be included in packet
     * @param bytesCount    amount of data to be sent
     */
    virtual Result_e SendPacket(const Rs485Packet* pOutPacket, Rs485Packet* pInPacket)
    {
        {
            /* Prepare RX packet decoder for receive */
            rxPacketDecoder_.Initialize();

            /* Make packet, apply SLIP and put to buffer */
            DataLength_t packetLength = preparePacket(pOutPacket);

            /* Send packet via serial line */
            SendBuffer(packetLength);
        }

        if ( mode_ == ROLE_MASTER )
        {
            /* Wait before all data went from port */
            while (!IsTxReady());

            /* Save timestamp when receiving timeout started */
            TickCounter_t startTicks = getCurrentTicks();

            DataLength_t packetLength = 0;

            /* Wait until packet received or timeout elapsed */
            while ( !(rxPacketDecoder_.GetPacketLength(&packetLength)) && (getCurrentTicks() - startTicks < RS485_ANSWER_TIMEOUT));

            /* Answer received  */
            if ( packetLength > 0 )
            {
                if (CheckCRC(rxBuffer_, packetLength))
                {
                    /* Set data from packet */
                    FillPacket(pInPacket, rxBuffer_);

                    return RESULT_OK;
                }
                else
                {
                    /* Invalid CRC */
                    return RESULT_INVALID_CRC;
                }
            }
            else
            {
                /* No response during timeout */
                return RESULT_TIMEOUT;
            }
        }

        return RESULT_OK;
    }

    /**
     * Configures UART port, speed, parity, interrupts, etc.
     * The function should have additional parameters like Speed, Parity, etc
     * to simplify, just omitting them and hardcode common parameters
     */
    virtual void InitPort() = 0;

    inline void ClearRxBuffer()
    {
        rxBufferDataCount_ = 0;
    }

    virtual bool IsTxReady() = 0;

    inline bool IsRxReady()
    {
        return (rxBufferDataCount_ > 0);
    }

    virtual ~Rs485Transmitter()
    {

    }

protected:
    uint8_t txBuffer_[MAX_PACKET_LENGTH];
    uint8_t rxBuffer_[MAX_PACKET_LENGTH];

    DataLength_t rxBufferDataCount_;
    DataLength_t txBufferDataCount_;

    DataLength_t txBufferPtr_;    

    Rs485PacketDecoder rxPacketDecoder_;

    const Rs485TransmitterRole_e mode_;

    virtual void turnOffInterrupts() = 0;
    virtual void turnOnInterrupts() = 0;

    /**
     * Sends packet of data using hardware
     * @param pData         data to be sent
     * @param dataLength    amount of bytes
     */
    virtual void SendBuffer(DataLength_t dataLength) = 0;

    /**
     * Prepares and puts to output buffer packet with specified parameters
     * @param deviceId      address of device
     * @param cmd           command
     * @param pData         data to be included
     * @param bytesCount    amount of data in packet
     * @return  total length of packet
     */
    virtual DataLength_t preparePacket(const Rs485Packet* pOutPacket)
    {
        uint8_t *pCursor = txBuffer_;

        /* Put frame marker */
        *pCursor = SLIP_END;
        pCursor++;

        /* Put deviceId */
        pCursor += putEscaped(pCursor, &pOutPacket->deviceId, sizeof(pOutPacket->deviceId));

        /* Put command */
        pCursor += putEscaped(pCursor, &pOutPacket->cmd, sizeof(pOutPacket->cmd));

        /* PutData */
        pCursor += putEscaped(pCursor, pOutPacket->pData, sizeof(pOutPacket->bytesCount));

        /* Calculate and put CRC */
        uint16_t crcResult = CRC_INITAL_VALUE;
        crcResult = crc16(crcResult, &pOutPacket->deviceId, sizeof(pOutPacket->deviceId));
        crcResult = crc16(crcResult, &pOutPacket->cmd, sizeof(pOutPacket->cmd));
        crcResult = crc16(crcResult, pOutPacket->pData, pOutPacket->bytesCount);
        pCursor += putEscaped(pCursor, &crcResult, sizeof(crcResult));

        /* Put frame marker */
        *pCursor = SLIP_END;
        pCursor++;

        return (pCursor - txBuffer_);
    }

    /**
     * Escapes data using SLIP rules and puts data to buffer
     * @param pDst
     * @param pData
     * @param bytesCount
     * @return
     */
    virtual DataLength_t putEscaped(uint8_t *pDst, const void* pData, DataLength_t bytesCount)
    {
        /* TODO Here will be a loop to put check every symbol, put it or put 2 bytes instead
         * according to SLIP rules */

        return bytesCount;
    }

    /**
     * Adds byte to packet decoder
     * @param byte  received byte
     */
    void AddRxByte(uint8_t byte)
    {
        rxPacketDecoder_.PutByte(byte);

        /* In Slave mode, call callback with received packet */
        if (mode_ == ROLE_SLAVE)

        {
            DataLength_t packetLength = 0;
            if (rxPacketDecoder_.GetPacketLength(&packetLength)
                    && CheckCRC(rxBuffer_, packetLength))
            {
                Rs485Packet packet;

                /* Raise callback with received data */
                FillPacket(&packet, rxBuffer_);
            }
            else
            {
                /* If CRC not valid - ignore incoming packet */
            }
        }
    }

    /**
     * Fills packet with data from buffer
     * @param pPacket   output packet
     * @param pBuffer   incoming data
     */
    void FillPacket(Rs485Packet *pPacket, uint8_t *pBuffer)
    {
        uint8_t *pCursor = pBuffer;

        pPacket->deviceId = *pCursor;
        pCursor += (sizeof(pPacket->deviceId));

        pPacket->cmd = *pCursor;
        pCursor += (sizeof(pPacket->cmd));

        pPacket->bytesCount = *pCursor;
        pCursor += (sizeof(pPacket->bytesCount));

        memcpy(pPacket->pData, pCursor, pPacket->bytesCount);
    }


    /**
     * Checks CRC in packet
     * @param pBuffer       packet
     * @param packetLength  amount of data
     * @return  true if CRC OK
     */
    bool CheckCRC(uint8_t *pBuffer, DataLength_t packetLength)
    {
        /* Check CRC of packet */
        CrcValue_t crcCalc = crc16(CRC_INITAL_VALUE, pBuffer, packetLength - sizeof(CrcValue_t));
        CrcValue_t crcPacket = *(pBuffer + packetLength - sizeof(CrcValue_t));

        return crcCalc == crcPacket;
    }

    Rs485Transmitter(Rs485TransmitterRole_e mode) : rxPacketDecoder_(rxBuffer_, sizeof(rxBuffer_)), mode_(mode)
    {
        rxBufferDataCount_ = 0;
        txBufferDataCount_ = 0;

        txBufferPtr_ = 0;
    }

    virtual void OnSlaveModePacketRecieved(Rs485Packet *pPacket) = 0;
};

#endif /* RS485TRANSMITTER_H */
