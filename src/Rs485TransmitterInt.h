#ifndef RS485TRANSMITTERINT_H
#define RS485TRANSMITTERINT_H

#include "Rs485Transmitter.h"

/**
 * Rs485 Transmitter implemented using interrupt handlers.
 *
 * You should implement another class derived from Rs485Transmitter, if you want to use DMA
 * or fully-blocking implementation (awaiting on serial port flags).
 */
class Rs485TransmitterInt : public Rs485Transmitter
{
public:

    /**
     * Called from interrupt handler, when ready to send next bytes
     */
    virtual inline bool OnByteSent()
    {
        if ( txBufferPtr_ < txBufferDataCount_ )
        {
            writeByte(txBuffer_[txBufferPtr_++]);
            return true;
        }
        else 
        {
            txBufferPtr_ = 0;
            txBufferDataCount_ = 0;
            return false;
        }
    }

    /**
     * Called from interrupt handler, when byte is ready to be read
     */
    virtual inline void OnByteReceived()
    {
        uint8_t reg = readByte();

        AddRxByte(reg);
    }

protected:
    
    /**
     * Platform-depend function for reading from UART register
     * @return
     */
    virtual uint8_t readByte() = 0;

    /**
     * Platform-depend function for writing to UART register
     * @param data
     */
    virtual void writeByte(uint8_t data) = 0;

    void SendBuffer(DataLength_t dataLength)
    {
        if ( dataLength > 0 )
        {
            turnOffInterrupts();
            txBufferDataCount_ = dataLength;
            txBufferPtr_ = 0;
            writeByte(txBuffer_[txBufferPtr_++]);
            turnOnInterrupts();
        }
    }

    Rs485TransmitterInt(Rs485TransmitterRole_e mode) : Rs485Transmitter(mode)
    {

    }
};

#endif /* RS485TRANSMITTERINT_H */
