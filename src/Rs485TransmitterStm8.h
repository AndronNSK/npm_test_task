#ifndef RS485TRANSMITTERSTM8_H
#define RS485TRANSMITTERSTM8_H

#include "Rs485TransmitterInt.h"

#include "stm8s.h"
#include "stm8s_uart1.h"

/**
 * Implementation for STM8 MCU.
 *
 * Need to be checked what is more efficient and uses less code: inheritance for particular chip,
 * or passing an object with functions send/recv byte.
 */
class Rs485TransmitterStm8 : public Rs485TransmitterInt
{
public:
    void InitPort()
    {
//        halUART1_Init(115200UL,
//                        UART1_WORDLENGTH_8D,
//                        UART1_STOPBITS_1,
//                        UART1_PARITY_NO,
//                        UART1_SYNCMODE_CLOCK_DISABLE,
//                        UART1_MODE_TX_ENABLE);

        UART1_DeInit();
        UART1_Init(115200UL, UART1_WORDLENGTH_8D, UART1_STOPBITS_1, UART1_PARITY_NO,
                UART1_SYNCMODE_CLOCK_DISABLE, UART1_MODE_TXRX_ENABLE);
        
        UART1_ITConfig(UART1_IT_RXNE_OR, ENABLE);
        UART1_ITConfig(UART1_IT_TXE, ENABLE);
    }
    
    virtual bool IsTxReady()
    {
        return (txBufferDataCount_ == txBufferPtr_) && (UART1_GetFlagStatus(UART1_FLAG_TXE) == SET);
    }

    Rs485TransmitterStm8(Rs485TransmitterRole_e mode) : Rs485TransmitterInt(mode)
    {

    }

protected:

    inline uint8_t readByte()
    {
        return UART1_ReceiveData8();
    }

    inline void writeByte(uint8_t data)
    {
        UART1_SendData8((uint8_t) data);
    }

    inline void turnOffInterrupts()
    {
        disableInterrupts();
    }

    inline void turnOnInterrupts()
    {
        enableInterrupts();
    }

    void OnSlaveModePacketRecieved(Rs485Packet *pPacket);
};

#endif /* RS485TRANSMITTERSTM8_H */
