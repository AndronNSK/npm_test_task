#include "main_loop.h"

/* Here is an application defined implementation of packets received on Slave.
 * This function will be called from interrupt, so we should keep it in mind
 * when sending messages to main program */
void Rs485TransmitterStm8::OnSlaveModePacketRecieved(Rs485Packet *pPacket)
{
    /* Check is packet for current device, or may be broadcasting, etc... */
    if ( pPacket->deviceId != 11 )
    {
        return;
    }

    switch(pPacket->cmd)
    {
    case 1:
        break;
    case 2:
        break;
    default:
        break;
    }
}



Rs485TransmitterStm8 rs485(ROLE_MASTER);
/**
 * Example for Master device
 */
void mainLoop()
{
    while(1)
    {
        const char testString[] = "This is a test string\n\n";

        rs485.InitPort();
        
        Rs485Packet packet;
        packet.deviceId = 11;
        packet.cmd = 1; /* Some value, depends on protocol implementation.  */
        packet.bytesCount = sizeof(testString);
        packet.pData = (void *)testString;

        Rs485Packet answerPacket;
        
        /* Blocking call */
        Result_e res = rs485.SendPacket(&packet, &answerPacket);

        /* Below will be result handling, data analysing, etc  */
        
    }
}
