#ifndef MAIN_LOOP_H
#define MAIN_LOOP_H

#include "Rs485TransmitterStm8.h"

extern Rs485TransmitterStm8 rs485;

inline void onByteRecieved()
{
    rs485.OnByteReceived();
}

inline void onByteSent()
{
    if (!rs485.OnByteSent())
    {
        UART1_ITConfig(UART1_IT_TXE, DISABLE);
    }
}

void mainLoop();

#endif /* MAIN_LOOP_H */
